1.3.1
- Fix spacing for invalid word entered message

1.3.0
- Support for variable word length (1-25)

1.2.1
- Import termcolor into repo files

1.2.0
- Added color reveal animation

1.1.0
- Add colored emoji graph and word number to the end screen

1.0.0
